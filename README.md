1. Clonar el repo de intergram: 
```
git clone https://github.com/idoco/intergram.git
```
1. Levantar el contenedor:
```
docker-compose up -d
```
1. Acceder al bot en: localhost:8050
